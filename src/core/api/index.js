import axios from "axios";
import { api } from "../api";

// signup
export const signup = (data) => {
  return api.post("/users", data);
};

//get ALl users
export const getAllUser = () => {
  return api.get("/users");
};
// logout
// export const logout = () => {
//   return api.get("/users/logout");
// };
export const getAllAccountsData = () => {
  return axios.get("/accounts", {
    withCredentials: true,
    baseURL: "http://localhost:3001/api/v1",
  });
};

export const createNewAccount = (data) => {
  return axios.post("/accounts", data);
};

export const updateAccount = (data, id) => {
  return api.patch(`/accounts/${id}`, data);
};

export const createNewTransaction = (data) => {
  return api.post("/transactions", data);
};
export const geAllTransactions = (data) => {
  return api.get("/transactions", data);
};
