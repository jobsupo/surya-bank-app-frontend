import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const About = () => {
  const navigate = useNavigate();
  useEffect(() => {
    navigate("/contact");
  });
  return <>About</>;
};
export default About;
