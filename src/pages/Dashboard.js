import { Button, Col, Container, Row } from "react-bootstrap";
import CardComponent from "../components/Card";
import Title from "../components/Title";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getAllAccountsData } from "../core/api/index";
import { accountActions } from "../redux/account";
import { useNavigate } from "react-router-dom";

const Dashboard = () => {
  const navigate = useNavigate();
  const loggedUser = useSelector((state) => state.authReducer.loggedUser);
  const dispatch = useDispatch();
  const [total, setTotal] = useState();
  useEffect(() => {
    getAllAccounts();
  });

  const getAllAccounts = async () => {
    await getAllAccountsData()
      .then((res) => {
        dispatch(accountActions.accountList(res.data.data.data));
        dispatch(
          accountActions.accountTotal(
            res.data.data.data.reduce((n, { balance }) => n + balance, 0)
          )
        );
        const accs = res.data.data.data
          ?.filter((a) => a.user === loggedUser._id)
          .reduce((n, { balance }) => n + balance, 0);

        setTotal(accs);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Container>
      <Col>
        <Row className="m-3">
          <Col>
            <Title name={loggedUser.name} />
          </Col>
          <Col className="text-end">
            <Button
              variant="warning"
              onClick={(e) => {
                e.preventDefault();
                navigate("/create-new-account");
              }}
            >
              Create new account
            </Button>
          </Col>
        </Row>

        <Row>
          <Col className="m-5 d-flex justify-content-center">
            <CardComponent
              title="Bank Total"
              total={`$ ${total}`}
              text={"Displays the combined total of all your bank accounts"}
              link1="Check Accounts"
              link1Url={"/check-accounts"}
              param={{ action: "none" }}
            />
          </Col>
          <Col className="m-5 d-flex justify-content-center">
            <CardComponent
              title="Transactions"
              total={""}
              text={"Make transfers between accounts"}
              link1="transfer money"
              link2="All transactions"
              link2Url="/all-transactions"
              link1Url={"/transactions"}
              param={{ action: "none" }}
            />
          </Col>
          <Col className="m-5 d-flex justify-content-center">
            <CardComponent
              title="Withdraw"
              total={""}
              text={"Withdraw from selected accounts"}
              link1="Withdraw money"
              link1Url={"/check-accounts"}
              param={{ action: "Withdraw" }}
            />
          </Col>
          <Col className="m-5 d-flex justify-content-center">
            <CardComponent
              title="Deposits"
              total={""}
              text={"Deposit to a selected account"}
              link1="Deposit money"
              link1Url={"/check-accounts"}
              param={{ action: "Deposit" }}
            />
          </Col>
        </Row>
      </Col>
    </Container>
  );
};
export default Dashboard;
