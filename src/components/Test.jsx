// "use client";
// import { FormEvent, useState } from "react";
// import {
//   Button,
//   Col,
//   Container,
//   Form,
//   FormCheck,
//   FormControl,
//   FormGroup,
//   FormLabel,
//   FormText,
//   Row,
// } from "react-bootstrap";
// import { useDispatch, useSelector } from "react-redux";
// import { authActions } from "../redux/authentication";
// import { useNavigate } from "react-router-dom";

// const Login = () => {
//   const [email, setEmail] = useState();
//   const [password, setPassword] = useState();
//   const dispatch = useDispatch();
//   const navigate = useNavigate();

//   const onSubmit = (e) => {
//     e.preventDefault();
//     if (email === "Surya@gmail.com" && password === "123") {
//       dispatch(authActions.isLoggedIn());
//       // dispatch(
//         authActions.loggedUser({
//           _id: "65fbf3e890d438c4df57df3f",
//           name: "Surya",
//           email: "Surya@gmail.com",
//           password: "123",
//         })
//       );
//       navigate("/dashboard");
//     }
//     if (email === "smith@gmail.com" && password === "123") {
//       dispatch(authActions.isLoggedIn());
//       dispatch(
//         authActions.loggedUser({
//           _id: "65fc07adf08e71641e541a1d",
//           name: "Smith",
//           email: "smith@gmail.com",
//           password: "123",
//           role: "Manager",
//         })
//       );
//       navigate("/admin");
//     }

//     if (email === "John@gmail.com" && password === "123") {
//       dispatch(authActions.isLoggedIn());
//       dispatch(
//         authActions.loggedUser({
//           _id: "65fbf6e5ec352fbcccce6ab9",
//           name: "John",
//           email: "John@gmail.com",
//           password: "123",
//         })
//       );
//       navigate("/dashboard");
//     }
//   };

//   return (
//     <Container className=" ">
//       <Col>
//         <Row className="d-flex justify-content-center">
//           <Col>
//             <h3 className="p-5">Login</h3>
//           </Col>
//         </Row>
//         <Row>
//           <Col md={6} className="m-5 ">
//             <Form onSubmit={onSubmit}>
//               <FormGroup className="mb-3 " controlId="email">
//                 <FormLabel>Email</FormLabel>
//                 <FormControl
//                   type="email"
//                   onChange={(e) => {
//                     setEmail(e.target.value);
//                   }}
//                   placeholder="Ex: John Smith"
//                   value={email}
//                 />
//               </FormGroup>
//               <FormGroup className="mb-3" controlId="password">
//                 <FormLabel>Password</FormLabel>
//                 <Form.Control
//                   type="password"
//                   onChange={(e) => {
//                     setPassword(e.target.value);
//                   }}
//                   value={password}
//                   placeholder="Password"
//                 />
//               </FormGroup>

//               <Button variant="primary" type="submit">
//                 Login
//               </Button>
//             </Form>
//           </Col>
//         </Row>
//       </Col>
//     </Container>
//   );
// };
// export default Login;
