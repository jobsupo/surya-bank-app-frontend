import { Button, Form, FormControl, FormGroup } from "react-bootstrap";
import { updateAccount } from "../core/api/index";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";

import { accountActions } from "../redux/account";
import { useState } from "react";

const AccForm = ({
  it,
  action,

  updated,
  setUpdated,

  setUserAccs,
}) => {
  const loggedUser = useSelector((state) => state.authReducer.loggedUser);
  const dispatch = useDispatch();
  const [amountval, setAmountVal] = useState({});
  const getAllAccounts = async () => {
    await axios
      .get("/accounts", {
        withCredentials: true,
        baseURL: "http://localhost:3001/api/v1",
      })
      // await getAllAccountsData()
      .then((res) => {
        dispatch(accountActions.accountList(res.data.data.data));
        dispatch(
          accountActions.accountTotal(
            res.data.data.data.reduce((n, { balance }) => n + balance, 0)
          )
        );
        setUserAccs(
          res.data.data.data.filter((a) => loggedUser._id === a.user)
        );
        setTimeout(() => {
          setUpdated(false);
        }, 3000);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onSubmit = async (e, it) => {
    e.preventDefault();
    let data;
    if (action === "Withdraw") {
      data = {
        balance: Number(amountval?.oldAMount) - Number(amountval?.amount),
      };
    } else {
      data = {
        balance: Number(amountval?.oldAMount) + Number(amountval?.amount),
      };
    }

    await axios
      .patch(`/accounts/${amountval?.id}`, data, {
        withCredentials: true,
        baseURL: "http://localhost:3001/api/v1",
      })
      // await updateAccount(data, amountval.id)
      .then((res) => {
        console.log(res);
        setUpdated(true);
        getAllAccounts();
        setAmountVal({
          amount: 0,
          id: null,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handlAmount = (e, id, amount) => {
    const { value } = e.target;
    if (value > amount && action === "Withdraw") {
      setAmountVal({
        amount: 0,
        id: id,
      });
    }
    setAmountVal({
      amount: value,
      id: id,
      oldAMount: amount,
    });
    // setAmount((a) =>
    // setAmount(value);
    //   accountList?.map((list, index) =>
    //     index === id ? { ...list, a: value } : list
    //   )
    // );
    // userAccs.map((a) => (a._id === id ? (a.value = value) : ""));
  };
  return (
    <>
      <Form
        onSubmit={(e) => onSubmit(e, it)}
        aria-label="form"
        data-testid="form"
      >
        <FormGroup className="mb-3 " controlId="">
          <FormControl
            type="text"
            onChange={(e) => handlAmount(e, it?._id, it?.balance)}
            placeholder={`Max: ${it?.balance}`}
            data-testid="amountval"
            value={
              action === "Withdraw"
                ? it?._id === amountval?.id && it?.balance >= amountval?.amount
                  ? amountval?.amount
                  : ""
                : it?._id === amountval?.id
                ? amountval?.amount
                : ""
            }
          />
          {it?._id === amountval?.id &&
          it?.balance < amountval?.amount &&
          action === "Withdraw" ? (
            <small className="text-danger">
              X AMount should be less than ${it?.balance}
            </small>
          ) : (
            ""
          )}
          {it?._id === amountval?.id && updated && (
            <small className="text-success fw-bold">
              *** {action} Successfull ***
            </small>
          )}
        </FormGroup>{" "}
        <Button variant="danger" size={"sm"} type="submit">
          {action}
        </Button>
      </Form>
    </>
  );
};
export default AccForm;
