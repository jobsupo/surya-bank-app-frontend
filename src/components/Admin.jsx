import { useEffect } from "react";
import { getAllUser, getAllAccountsData } from "../core/api/index";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "../redux/user";
import { accountActions } from "../redux/account";
import { Col, Container, Row, Table } from "react-bootstrap";

const Admin = () => {
  const userList = useSelector((state) => state.userReducer.userList);
  const accountTotal = useSelector(
    (state) => state.accountReducer.accountsTotal
  );
  const accountList = useSelector((state) => state.accountReducer.accountList);
  const dispatch = useDispatch();

  useEffect(() => {
    getAllUsers();

    getAllAccounts();
  });
  const getAllAccounts = async () => {
    await getAllAccountsData()
      .then((res) => {
        dispatch(accountActions.accountList(res.data.data.data));
        dispatch(
          accountActions.accountTotal(
            res.data.data.data.reduce((n, { balance }) => n + balance, 0)
          )
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const getAllUsers = async () => {
    await getAllUser()
      .then((res) => {
        dispatch(userActions.userList(res.data.data.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Container>
      <Row>
        <Col className="text-end m-5">
          <h3>Total Bank Balance</h3>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>No of Accounts</th>
                <th>Total Deposit</th>
              </tr>
            </thead>
            <tbody>
              {userList?.map((e, index) => (
                <tr key={index}>
                  <td>{index}</td>
                  <td>{e.name}</td>
                  <td>{accountList?.filter((a) => a.user === e._id).length}</td>
                  <td>
                    {accountList
                      ?.filter((a) => a.user === e._id)
                      .reduce((n, { balance }) => n + balance, 0)}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
};
export default Admin;
