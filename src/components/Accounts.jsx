import React from "react";
import {
  Button,
  Col,
  Container,
  Form,
  FormControl,
  FormGroup,
  Row,
  Table,
} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { getAllAccountsData, updateAccount } from "../core/api/index";
import { accountActions } from "../redux/account";
import AccForm from "./AccForm";

const Accounts = () => {
  const dispatch = useDispatch();

  const loggedUser = useSelector((state) => state.authReducer.loggedUser);
  const [amountval, setAmountVal] = useState({});
  const [updated, setUpdated] = useState(false);
  const location = useLocation();
  const action = location?.state?.action; //destructuring throws testing error
  const accountList = useSelector((state) => state.accountReducer.accountList);
  const [userAccs, setUserAccs] = useState(
    accountList.filter((a) => loggedUser._id === a.user)
  );

  useEffect(() => {}, [userAccs]);

  const getAllAccounts = async () => {
    await getAllAccountsData()
      .then((res) => {
        dispatch(accountActions.accountList(res.data.data.data));
        dispatch(
          accountActions.accountTotal(
            res.data.data.data.reduce((n, { balance }) => n + balance, 0)
          )
        );
        setUserAccs(
          res.data.data.data.filter((a) => loggedUser._id === a.user)
        );
        setTimeout(() => {
          setUpdated(false);
        }, 3000);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // const onSubmit = async (e, it) => {
  //   e.preventDefault();
  //   let data;
  //   if (action === "Withdraw") {
  //     data = {
  //       balance: Number(amountval.oldAMount) - Number(amountval.amount),
  //     };
  //   } else {
  //     data = {
  //       balance: Number(amountval.oldAMount) + Number(amountval.amount),
  //     };
  //   }

  //   await updateAccount(data, amountval.id)
  //     .then((res) => {
  //       console.log(res);
  //       setUpdated(true);
  //       getAllAccounts();
  //       setAmountVal({
  //         amount: 0,
  //         id: null,
  //       });
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };
  // const handlAmount = (e, id, amount) => {
  //   const { value } = e.target;
  //   if (value > amount && action === "Withdraw") {
  //     setAmountVal({
  //       amount: 0,
  //       id: id,
  //     });
  //   }
  //   setAmountVal({
  //     amount: value,
  //     id: id,
  //     oldAMount: amount,
  //   });
  //   // setAmount((a) =>
  //   // setAmount(value);
  //   //   accountList?.map((list, index) =>
  //   //     index === id ? { ...list, a: value } : list
  //   //   )
  //   // );
  //   // userAccs.map((a) => (a._id === id ? (a.value = value) : ""));
  // };

  return (
    <Container>
      <Col>
        <Row>
          <Col className="m-5">
            <h3>List of bank accounts</h3>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Acc Name</th>
                  <th>Balance</th>
                  {(action === "Withdraw" || action === "Deposit") && (
                    <th>Action</th>
                  )}
                </tr>
              </thead>
              <tbody>
                {userAccs?.map((it, index) => (
                  <tr key={index}>
                    <td>{index}</td>
                    <td>{it.name}</td>
                    <td>
                      {it.balance === 0 || it.balance === null ? 0 : it.balance}
                    </td>
                    {(action === "Withdraw" || action === "Deposit") && (
                      <td width="300">
                        {!(it.balance === 0 || it.balance === null) && (
                          <AccForm
                            it={it}
                            action={action}
                            // amountval={amountval}
                            updated={updated}
                            setUpdated={setUpdated}
                            // setAmountVal={setAmountVal}
                            setUserAccs={setUserAccs}
                          />
                        )}
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Col>
    </Container>
  );
};
export default Accounts;
