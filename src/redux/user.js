import { createSlice } from "@reduxjs/toolkit";

const initialState = { userList: [], accountsList: [], transactionsList: [] };

const userSlice = createSlice({
  name: "users",
  initialState: initialState,
  reducers: {
    userList(state, action) {
      state.userList = action.payload;
    },
    userAccounts(state, action) {
      state.accountsList = action.payload;
    },
    transactionList(state, action) {
      state.list = action.payload;
    },
  },
});

export const userActions = userSlice.actions;
export default userSlice.reducer;
