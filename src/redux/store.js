import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./user";
import authReducer from "./authentication";
import accountReducer from "./account";

const store = configureStore({
  reducer: { userReducer, authReducer, accountReducer },
});

export default store;
