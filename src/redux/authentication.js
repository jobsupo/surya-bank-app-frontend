import { createSlice } from "@reduxjs/toolkit";

const initialState = { isLoggedIn: false, loggedUser: {} };

const authSlice = createSlice({
  name: "Authentication",
  initialState: initialState,
  reducers: {
    isLoggedIn(state) {
      state.isLoggedIn = true;
    },
    loggedUser(state, action) {
      state.loggedUser = action.payload;
    },
    logout(state) {
      state.isLoggedIn = false;
    },
  },
});

export const authActions = authSlice.actions;
export default authSlice.reducer;
