import { render, screen, fireEvent } from "@testing-library/react";
import AccForm from "../components/AccForm";
import store from "../redux/store";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { act } from "react-dom/test-utils";
import axios from "axios";

jest.mock("axios");
jest.mock("../core/api/index");
const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <AccForm />
      <Routes>
        <Route path="/dashboard">Dashboard</Route>
      </Routes>
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );

  return component;
};
describe("AccForm component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it("Should render a new account form", () => {
    expect(screen.getByRole("form")).toBeInTheDocument();
  });

  it("Should render Account Type  text field", () => {
    expect(screen.getByTestId("amountval"));
  });

  it("Should render Create Account button", () => {
    expect(screen.getByRole("button")).toBeInTheDocument();
  });

  //***********interaction testing

  it("Should render Create Account button", async () => {
    axios.patch = jest.fn().mockResolvedValue({
      data: [
        {
          userId: 1,
          id: 1,
          title: "test",
        },
      ],
    });
    await act(async () => {
      const amount = screen.getByTestId("amountval");
      fireEvent.change(amount, { target: { value: "500" } });
      expect(amount).toHaveValue("500");
      const button = screen.getByRole("button");
      fireEvent.click(button);
    });

    expect(axios.patch).toHaveBeenCalledTimes(1);
    // expect(title).toEqual("test");
    //
    // axios.get("https://www.google.com");
    expect(window.location.pathname).toBe("/");
  });
});
