import { render, screen, fireEvent } from "@testing-library/react";
import Accounts from "../components/Accounts";
import store from "../redux/store";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { act } from "react-dom/test-utils";

import axios from "axios";

jest.mock("axios");
jest.mock("../core/api/index");
const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <Accounts />
      <Routes>
        <Route path="/dashboard">Dashboard</Route>
      </Routes>
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );

  return component;
};
describe("Account list component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it("Should render h3 heading with text", () => {
    expect(screen.getByRole("heading", { level: 3 })).toHaveTextContent(
      /List of bank accounts/i
    );
  });
});
